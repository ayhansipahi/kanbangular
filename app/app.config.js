(function () {
	'use strict';

	/**
	 * @ngdoc configuration file
	 * @name app.config:config
	 * @description
	 * # Config and run block
	 * Configutation of the app
	 */


	angular
		.module('kanbangular')
		.config(configure)
		.config(material)
		.run(runBlock);

	configure.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider'];

	function configure($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
		$locationProvider.hashPrefix('!');
		// This is required for Browser Sync to work poperly
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		$urlRouterProvider
			.otherwise('/dashboard');
	}

	material.$inject = ['$mdThemingProvider'];

	function material($mdThemingProvider) {

		$mdThemingProvider.theme('default')
		$mdThemingProvider.theme('input')
			.primaryPalette('grey')
			.dark();

	}

	runBlock.$inject = ['$rootScope'];

	function runBlock($rootScope) {
		'use strict';
	}


})();
