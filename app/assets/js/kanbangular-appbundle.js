/*!
* kanbangular - v0.0.1 - MIT LICENSE 2016-09-03. 
* @author Ayhan Sipahi
*/
(function() {
	'use strict';

	/**
	* @ngdoc index
	* @name app
	* @description
	* # app
	*
	* Main module of the application.
	*/

	angular.module('kanbangular', [
		'ngResource',
		'ngAria',
	 	'ngMaterial',
		'ngMdIcons',
		'ngMessages',
		'ngCookies',
		'ngAnimate',
		'ngSanitize',
		'ui.router',
		'dndLists',
		'LocalStorageModule',
		'board'
	]);
})();

(function () {
	'use strict';

	/**
	 * @ngdoc configuration file
	 * @name app.config:config
	 * @description
	 * # Config and run block
	 * Configutation of the app
	 */


	angular
		.module('kanbangular')
		.config(configure)
		.config(material)
		.run(runBlock);

	configure.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider'];

	function configure($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
		$locationProvider.hashPrefix('!');
		// This is required for Browser Sync to work poperly
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		$urlRouterProvider
			.otherwise('/dashboard');
	}

	material.$inject = ['$mdThemingProvider'];

	function material($mdThemingProvider) {

		$mdThemingProvider.theme('default')
		$mdThemingProvider.theme('input')
			.primaryPalette('grey')
			.dark();

	}

	runBlock.$inject = ['$rootScope'];

	function runBlock($rootScope) {
		'use strict';
	}


})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.module:boardModule
	* @description
	* # boardModule
	* Module of the app
	*/

	angular.module('board', []);
})();

'use strict';

	/**
	* @ngdoc function
	* @name app.route:boardRoute
	* @description
	* # boardRoute
	* Route of the app
	*/

angular.module('kanbangular')
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider

			.state('board', {
				url: '',
				abstract: true,
				templateUrl: 'app/modules/board/board.html',
				controller: 'BoardCtrl',
				controllerAs: 'vm'
			})
			.state('board.dashboard', {
				url:'/dashboard',
				templateUrl: 'app/modules/board/dashboard.html'
			});

	}]);

(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:boardService
	* @description
	* # boardService
	* Service of the app
	*/

	angular.module('kanbangular')
		.factory('boardService', boardService);

	boardService.$inject = ['lsService'];

	function boardService(lsService) {

		return {
			set: set,
			init: init
		};

		function set(data) {
			return lsService.set(data);
		}

		function init() {
			return lsService.get();
		}
	}

})();

(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:labelService
	 * @description
	 * # labelService
	 * Service of the app
	 */

	angular.module('kanbangular')
		.factory('labelService', lsService);


	function lsService() {
		const labels = [
			{
				"name": "User Story",
				"color": "#01cc00"
			},
			{
				"name": "Defect",
				"color": "#fe0000"
			},
			{
				"name": "Task",
				"color": "#0065b3"
			},
			{
				"name": "Feature",
				"color": "#ff7f00"
			}
		];

		return {
			labels: labels
		};

	}

})();

(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:localstorageService
	 * @description
	 * # localstorageService
	 * Service of the app
	 */

	angular.module('kanbangular')
		.factory('lsService', lsService);

	lsService.$inject = ['localStorageService'];

	function lsService(localStorageService) {
		const key = "data";
		return {
			set: set,
			get: get
		};

		function set(data) {
			return localStorageService.set(key, JSON.stringify(data));
		}

		function get() {
			return  JSON.parse(localStorageService.get(key)) || [];
		}
	}

})();

(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:BoardCtrl
	* @description
	* # BoardCtrl
	* Controller of the app
	*/

	angular
		.module('kanbangular')
		.controller('BoardCtrl', Board);

	Board.$inject = ['$rootScope', '$scope', '$window','$mdDialog', '$mdToast', 'boardService'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Board($rootScope, $scope, $window, $mdDialog, $mdToast, boardService) {
		/*jshint validthis: true */

		var originatorEv;
		$scope.openMenu = function($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		$scope.listStyle = {
			height: ($window.innerHeight - 64) + 'px'
		};
		$window.addEventListener('resize', onResize);
		function onResize() {
			$scope.listStyle.height = ($window.innerHeight - 64) + 'px';
			if (!$scope.$root.$$phase) $scope.$digest();
		}

		$scope.models = boardService.init();


		$scope.data = JSON.stringify($scope.models);

		$scope.addData = function() {
			boardService.set(JSON.parse($scope.data));
			$scope.model = boardService.init();
		};

		/** List functions **/
		$scope.addList = function (ev) {
			$mdDialog.show({
				controller: 'ColumnCtrl',
				templateUrl: 'app/modules/board/dialogs/column.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals:{
					name: null
				}
			})
			.then(function(name) {
				var list = {
					id: (new Date().getTime()).toString(36),
					name: name,
					items: []
				};
				$scope.models.push(list);
			});
		};
		$scope.editList = function(ev, id){
			var list = $scope.models.filter(function(el,i) {
				return el.id == id;
			});
			$mdDialog.show({
				controller: 'ColumnCtrl',
				templateUrl: 'app/modules/board/dialogs/column.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals:{
					name: list[0].name
				}
			})
			.then(function(name) {
				list[0].name = name;
			});
		};

		$scope.deleteList = function(ev, data) {
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
				.title('Would you like to delete column '+data.name+'?')
				.textContent('All ('+ data.items.length +') cards in this column will be deleted.')
				.ariaLabel('Delete Column')
				.targetEvent(ev)
				.ok('Ok')
				.cancel('Cancel');
			$mdDialog.show(confirm).then(function() {
				$scope.models = $scope.models.filter(function(el, i) {
					return el.id !== data.id;
				});
			});
		};

		/** drag and drop functions **/
		$scope.getSelectedItemsIncluding = function(list, item) {
			item.selected = true;
			return list.items.filter(function(item) { return item.selected; });
		};

		$scope.onDragstart = function(list, event) {
			list.dragging = true;
			if (event.dataTransfer.setDragImage) {
				var img = new Image();
				img.src = 'app/assets/images/copy.png';
				event.dataTransfer.setDragImage(img, 0, 0);
			}
		};

		$scope.onDrop = function(list, items, index) {
			angular.forEach(items, function(item) { item.selected = false; });
			list.items = list.items.slice(0, index)
				.concat(items)
				.concat(list.items.slice(index));
			return true;
		};

		$scope.onMoved = function(list) {
			list.items = list.items.filter(function(item) { return !item.selected; });
		};

		/** Card Functions **/
		$scope.addItem = function(ev, list){
			$mdDialog.show({
				controller: 'CardCtrl',
				templateUrl: 'app/modules/board/dialogs/card.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals:{
					list: list,
					lists: $scope.models,
					item: false
				}
			})
			.then(function(scope) {
				var item = {
					id: (new Date().getTime()).toString(36),
					title: scope.title,
					description: scope.description,
					labels: scope.currentLabels,
					commends: []
				};
				$scope.models.filter(function(el,i) {
					return el.id == scope.currentList;
				})[0].items.push(item);
			});
		};
		$scope.editItem = function(ev, list, item){
			var listId = list;
			$mdDialog.show({
				controller: 'CardCtrl',
				templateUrl: 'app/modules/board/dialogs/card.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals:{
					list: list,
					lists: $scope.models,
					item: item
				}
			})
			.then(function(scope) {
				if (scope.currentList == listId){
					var orgItem = $scope.models.filter(function(el,i) {
						return el.id == listId;
					})[0].items.filter(function(el,i){
						return el.id == item.id;
					})[0];

					orgItem.title =  scope.title;
					orgItem.description= scope.description;
					orgItem.labels= scope.currentLabels;
				} else {
					angular.forEach($scope.models, function(el,i){
						el.items = el.items.filter(function(el, i) {
							return el.id !== item.id
						});
					});

					item.title =  scope.title;
					item.description= scope.description;
					item.labels= scope.currentLabels;

					$scope.models.filter(function(el,i) {
						return el.id == scope.currentList;
					})[0].items.push(item);
				}
			});
		};
		$scope.deleteItem = function(ev, item){
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
				.title('Would you like to delete card?')
				.textContent('All card in this column will be deleted.')
				.ariaLabel('Delete Column')
				.targetEvent(ev)
				.ok('Ok')
				.cancel('Cancel');
			$mdDialog.show(confirm).then(function() {
				angular.forEach($scope.models, function(el,i){
					el.items = el.items.filter(function(el, i) {
						return el.id !== item
					});
				});
			});
		}

		/** save on any change **/
		$scope.$watch('models', function(model) {
			boardService.set(model);
		}, true);

		$rootScope.$watch('models', function(val){
			$scope.models = boardService.init();
		});
	}
})();

(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:cardCtrl
	 * @description
	 * # cardCtrl
	 * Controller of the card
	 */

	angular
		.module('kanbangular')
		.controller('CardCtrl', Card);

	Card.$inject = ['$scope', '$mdDialog', 'labelService', 'list', 'lists','item'];

	function Card($scope, $mdDialog, labelService,list, lists, item) {
		$scope.dialogtitle = "Add Card";
		$scope.buttonLabel = "Add";
		$scope.lists = lists;
		$scope.labels = labelService.labels;

		if (list){
			$scope.currentList = list
		}
		if (item){
			$scope.dialogtitle = "Edit Card";
			$scope.buttonLabel = "Save";
			$scope.title = item.title;
			$scope.description = item.description;
			$scope.currentLabels = item.labels;
		}
		$scope.cancel = function() {
			$mdDialog.cancel();
		};
		$scope.add = function() {
			$mdDialog.hide($scope);
		};
	}
})();

(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:columnCtrl
	 * @description
	 * # columnCtrl
	 * Controller of the column
	 */

	angular
		.module('kanbangular')
		.controller('ColumnCtrl', Column);

	Column.$inject = ['$scope', '$mdDialog', 'name'];

	function Column($scope, $mdDialog, name) {
		if (name && name.length > 0){
			$scope.name = name;
			$scope.buttonLabel = "Save";
			$scope.dialogtitle = "Edit Column";
		} else {
			$scope.buttonLabel = "Add";
			$scope.dialogtitle = "Add Column";
		}
		$scope.cancel = function() {
			$mdDialog.cancel();
		};
		$scope.add = function(answer) {
			$mdDialog.hide($scope.name);
		};
	}
})();

(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:LayoutCtrl
	* @description
	* # LayoutCtrl
	* Controller of the app
	*/

	angular
		.module('kanbangular')
		.controller('LayoutCtrl', Layout);

	Layout.$inject = ['$rootScope', '$mdToast', '$mdDialog','lsService'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Layout($rootScope, $mdToast, $mdDialog, lsService) {
		/*jshint validthis: true */
		var vm = this;

		var originatorEv;
		vm.openMenu = function ($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		vm.export = function(ev){
			$mdDialog.show({
				controller: fileImport,
				templateUrl: 'app/modules/layouts/main-page/import.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals: {type:"export"}
			})
		};
		vm.import= function(ev){
			$mdDialog.show({
				controller: fileImport,
				templateUrl: 'app/modules/layouts/main-page/import.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals: {type:"import"}
			})
				.then(function(json) {
					try{
						var j = JSON.parse(json);
						lsService.set(j);
						$mdToast.showSimple("Data import successful.");
						$rootScope.models = j;
					} catch(err){
						$mdToast.showSimple(err.message);
					}
				});
		};

		fileImport.$inject = ['$scope', '$mdDialog','type'];

		function fileImport($scope, $mdDialog, type) {
			if(type == 'import'){
				$scope.title = "Import Data"
				$scope.message = "Paste your data here"
			} else {
				$scope.title = "Export Data"
				$scope.message = "Get your data here";
				$scope.data = angular.toJson(lsService.get());
			}
			$scope.type = type;
			$scope.cancel = function() {
				$mdDialog.cancel();
			};
			$scope.import = function() {
				$mdDialog.hide($scope.data);
			};
		}

	}

})();
