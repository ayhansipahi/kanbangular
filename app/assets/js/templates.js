angular.module('kanbangular').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/modules/board/board.html',
    "<div layout=\"column\" class=\"relative\" layout-fill role=\"main\" ng-controller=\"LayoutCtrl as layout\" ng-cloak>\n" +
    "    <md-toolbar ng-show=\"!showSearch\">\n" +
    "        <div class=\"md-toolbar-tools\">\n" +
    "            <h3>Kanbangular</h3>\n" +
    "            <span flex></span>\n" +
    "            <!--<md-button aria-label=\"Search\" ng-click=\"showSearch = !showSearch\">-->\n" +
    "                <!--<ng-md-icon icon=\"search\"></ng-md-icon>-->\n" +
    "            <!--</md-button>-->\n" +
    "            <md-menu>\n" +
    "                <md-button aria-label=\"Open Settings\" ng-click=\"layout.openMenu($mdOpenMenu, $event)\">\n" +
    "                    <md-icon> more_vert </md-icon>\n" +
    "                </md-button>\n" +
    "                <md-menu-content width=\"4\">\n" +
    "                    <md-menu-item>\n" +
    "                        <md-button ng-click=\"layout.import($event)\">\n" +
    "                            <md-icon>file_upload</md-icon>\n" +
    "                            Import data\n" +
    "                        </md-button>\n" +
    "                    </md-menu-item>\n" +
    "                    <md-menu-item>\n" +
    "                        <md-button ng-click=\"layout.export()\">\n" +
    "                            <md-icon>file_download</md-icon>\n" +
    "                            Export data\n" +
    "                        </md-button>\n" +
    "                    </md-menu-item>\n" +
    "                </md-menu-content>\n" +
    "            </md-menu>\n" +
    "        </div>\n" +
    "    </md-toolbar>\n" +
    "    <!--<md-toolbar ng-show=\"showSearch\">-->\n" +
    "        <!--<div class=\"md-toolbar-tools\">-->\n" +
    "            <!--<md-button ng-click=\"showSearch = !showSearch\" aria-label=\"Back\">-->\n" +
    "                <!--<ng-md-icon icon=\"arrow_back\"></ng-md-icon>-->\n" +
    "            <!--</md-button>-->\n" +
    "            <!--<h3 flex=\"10\">-->\n" +
    "                <!--Back-->\n" +
    "            <!--</h3>-->\n" +
    "            <!--<md-input-container flex md-theme=\"input\" id=\"searchInput\">-->\n" +
    "                <!--<input ng-model=\"search.who\" placeholder=\"Search ...\">-->\n" +
    "            <!--</md-input-container>-->\n" +
    "        <!--</div>-->\n" +
    "    <!--</md-toolbar>-->\n" +
    "    <md-content layout=\"column\" flex md-scroll-y>\n" +
    "        <div ui-view layout=\"column\" flex></div>\n" +
    "    </md-content>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/board/dashboard.html',
    "<div flex layout=\"column\" layout-align=\"start none\">\n" +
    "	<div layout=\"column\">\n" +
    "		<md-virtual-repeat-container id=\"horizontal-container\" md-orient-horizontal ng-style=\"listStyle\">\n" +
    "			<div md-virtual-repeat=\"list in models\" class=\"repeated-item column\" md-whiteframe=\"1\" layout=\"column\" flex\n" +
    "				 layout-align=\"start none\">\n" +
    "				<div layout=\"row\" layout-align=\" center\">\n" +
    "					<span class=\"title\">{{list.name}} ({{list.items.length || 0}})</span>\n" +
    "					<span flex></span>\n" +
    "					<md-menu>\n" +
    "						<md-button class=\"md-icon-button\" aria-label=\"Open Settings\"\n" +
    "								   ng-click=\"openMenu($mdOpenMenu, $event)\">\n" +
    "							<md-icon> more_vert</md-icon>\n" +
    "						</md-button>\n" +
    "						<md-menu-content width=\"4\">\n" +
    "							<md-menu-item>\n" +
    "								<md-button ng-click=\"addItem($event,list.id)\">\n" +
    "									<md-icon>add</md-icon>\n" +
    "									Add Card\n" +
    "								</md-button>\n" +
    "							</md-menu-item>\n" +
    "							<md-menu-item>\n" +
    "								<md-button ng-click=\"editList($event, list.id)\">\n" +
    "									<md-icon>mode_edit</md-icon>\n" +
    "									Edit Column\n" +
    "								</md-button>\n" +
    "							</md-menu-item>\n" +
    "							<md-menu-divider></md-menu-divider>\n" +
    "							<md-menu-item>\n" +
    "								<md-button ng-click=\"deleteList($event,list)\">\n" +
    "									<md-icon>delete_forever</md-icon>\n" +
    "									Delete Column\n" +
    "								</md-button>\n" +
    "							</md-menu-item>\n" +
    "						</md-menu-content>\n" +
    "					</md-menu>\n" +
    "				</div>\n" +
    "				<div class=\"cards\" dnd-list dnd-drop=\"onDrop(list, item, index)\">\n" +
    "					<md-card class=\"text-center\"\n" +
    "							 ng-repeat=\"item in list.items\"\n" +
    "							 dnd-draggable=\"getSelectedItemsIncluding(list, item)\"\n" +
    "							 dnd-dragstart=\"onDragstart(list, event)\"\n" +
    "							 dnd-moved=\"onMoved(list)\"\n" +
    "							 dnd-dragend=\"list.dragging = false\"\n" +
    "							 dnd-selected=\"item.selected = !item.selected\"\n" +
    "							 ng-class=\"{'selected': item.selected}\"\n" +
    "							 ng-hide=\"list.dragging && item.selected\"\n" +
    "					>\n" +
    "						<md-card-header>\n" +
    "							<md-card-header-text>\n" +
    "								<span class=\"md-title\">{{item.title | limitTo:16}}</span>\n" +
    "								<span class=\"md-subhead\">{{item.description | limitTo:32}}</span>\n" +
    "							</md-card-header-text>\n" +
    "							<span flex></span>\n" +
    "							<md-menu>\n" +
    "								<md-button class=\"md-icon-button\" aria-label=\"Open Settings\"\n" +
    "										   ng-click=\"openMenu($mdOpenMenu, $event)\">\n" +
    "									<md-icon>more_vert</md-icon>\n" +
    "								</md-button>\n" +
    "								<md-menu-content width=\"4\">\n" +
    "									<md-menu-item>\n" +
    "										<md-button ng-click=\"editItem($event, list.id, item)\">\n" +
    "											<md-icon>mode_edit</md-icon>\n" +
    "											Edit Card\n" +
    "										</md-button>\n" +
    "									</md-menu-item>\n" +
    "									<md-menu-divider></md-menu-divider>\n" +
    "									<md-menu-item>\n" +
    "										<md-button ng-click=\"deleteItem($event, item.id)\">\n" +
    "											<md-icon>delete_forever</md-icon>\n" +
    "											Delete Card\n" +
    "										</md-button>\n" +
    "									</md-menu-item>\n" +
    "								</md-menu-content>\n" +
    "							</md-menu>\n" +
    "						</md-card-header>\n" +
    "						<md-card-actions layout=\"row\" layout-align=\"end center\">\n" +
    "							<div class=\"\" aria-label=\"Card Label\" ng-repeat=\"l in item.labels\">\n" +
    "								<md-tooltip>{{l.name}}</md-tooltip>\n" +
    "								<ng-md-icon icon=\"label\" ng-style=\"{color:l.color}\"></ng-md-icon>\n" +
    "							</div>\n" +
    "							<span flex></span>\n" +
    "						</md-card-actions>\n" +
    "					</md-card>\n" +
    "				</div>\n" +
    "				<div layout=\"column\">\n" +
    "					<md-button aria-label=\"Add Card\" ng-click=\"addItem($event, list.id)\">\n" +
    "						<md-tooltip>Add Card</md-tooltip>\n" +
    "						<ng-md-icon icon=\"add\" aria-label=\"Add Card\"></ng-md-icon>\n" +
    "					</md-button>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<md-button class=\"repeated-item\" md-whiteframe=\"1\" ng-click=\"addList($event)\">\n" +
    "				Add Column\n" +
    "			</md-button>\n" +
    "		</md-virtual-repeat-container>\n" +
    "	</div>\n" +
    "	<div id=\"addMenu\" layout=\"row\" layout-align=\"center center\">\n" +
    "		<md-fab-speed-dial md-direction=\"up\" class=\"md-scale\">\n" +
    "			<md-fab-trigger>\n" +
    "				<md-button aria-label=\"menu\" class=\"md-fab md-primary\">\n" +
    "					<ng-md-icon icon=\"menu\"></ng-md-icon>\n" +
    "				</md-button>\n" +
    "			</md-fab-trigger>\n" +
    "			<md-fab-actions>\n" +
    "				<md-button aria-label=\"Add Card\" class=\"md-fab md-raised md-mini\" ng-click=\"addItem($event, false)\">\n" +
    "					<md-tooltip md-direction=\"left\">Add Card</md-tooltip>\n" +
    "					<ng-md-icon icon=\"view_agenda\" aria-label=\"Add Card\"></ng-md-icon>\n" +
    "				</md-button>\n" +
    "				<md-button aria-label=\"Add Column\" class=\"md-fab md-raised md-mini\" ng-click=\"addList()\">\n" +
    "					<md-tooltip md-direction=\"left\">Add Column</md-tooltip>\n" +
    "					<ng-md-icon icon=\"view_column\" aria-label=\"Add Column\"></ng-md-icon>\n" +
    "				</md-button>\n" +
    "			</md-fab-actions>\n" +
    "		</md-fab-speed-dial>\n" +
    "	</div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/board/dialogs/card.html',
    "<md-dialog\n" +
    "	aria-label=\"Profile\">\n" +
    "	<form name=\"cardForm\" novalidate>\n" +
    "		<md-toolbar>\n" +
    "			<div class=\"md-toolbar-tools\">\n" +
    "				<h2>{{dialogtitle}}</h2>\n" +
    "				<span flex></span>\n" +
    "				<md-button class=\"md-icon-button\" ng-click=\"cancel()\">\n" +
    "					<md-icon aria-label=\"Close dialog\">close</md-icon>\n" +
    "				</md-button>\n" +
    "			</div>\n" +
    "		</md-toolbar>\n" +
    "		<md-dialog-content style=\"width:600px;max-width:100%; max-height:810px; \">\n" +
    "			<md-content class=\"md-padding\">\n" +
    "				<form name=\"userForm\" novalidate>\n" +
    "					<md-input-container class=\"md-block\">\n" +
    "						<label>Title</label>\n" +
    "						<input ng-model=\"title\" required>\n" +
    "					</md-input-container>\n" +
    "					<md-input-container class=\"md-block\">\n" +
    "						<label>Description</label>\n" +
    "						<textarea ng-model=\"description\" md-maxlength=\"150\" rows=\"5\" md-select-on-focus></textarea>\n" +
    "					</md-input-container>\n" +
    "					<md-input-container class=\"md-block\">\n" +
    "						<label>Labels</label>\n" +
    "						<md-select ng-model=\"currentLabels\"  multiple>\n" +
    "							<md-option ng-value=\"l\" ng-repeat=\"l in labels\" ><ng-md-icon icon=\"label\" ng-style=\"{color:l.color}\"></ng-md-icon>  {{l.name}} </md-option>\n" +
    "						</md-select>\n" +
    "					</md-input-container>\n" +
    "					<md-input-container class=\"md-block\">\n" +
    "						<label>Column</label>\n" +
    "						<md-select ng-model=\"currentList\" required>\n" +
    "							<md-option ng-value=\"l.id\"  ng-repeat=\"l in lists\">{{l.name}}</md-option>\n" +
    "						</md-select>\n" +
    "					</md-input-container>\n" +
    "				</form>\n" +
    "			</md-content>\n" +
    "		</md-dialog-content>\n" +
    "		<md-dialog-actions layout=\"row\">\n" +
    "			<span flex></span>\n" +
    "			<md-button ng-click=\"cancel()\">\n" +
    "				Cancel\n" +
    "			</md-button>\n" +
    "			<md-button ng-disabled=\"!cardForm.$valid\" ng-click=\"add()\" >\n" +
    "				{{buttonLabel}}\n" +
    "			</md-button>\n" +
    "		</md-dialog-actions>\n" +
    "	</form>\n" +
    "</md-dialog>\n"
  );


  $templateCache.put('app/modules/board/dialogs/column.html',
    "<md-dialog\n" +
    "	aria-label=\"Profile\">\n" +
    "	<form>\n" +
    "		<md-toolbar>\n" +
    "			<div class=\"md-toolbar-tools\">\n" +
    "				<h2>{{dialogtitle}}</h2>\n" +
    "				<span flex></span>\n" +
    "				<md-button class=\"md-icon-button\" ng-click=\"cancel()\">\n" +
    "					<md-icon aria-label=\"Close dialog\">close</md-icon>\n" +
    "				</md-button>\n" +
    "			</div>\n" +
    "		</md-toolbar>\n" +
    "		<md-dialog-content style=\"width:300px;max-width:100%; max-height:810px; \">\n" +
    "			<md-content class=\"md-padding\">\n" +
    "				<form name=\"userForm\" novalidate>\n" +
    "					<md-input-container class=\"md-block\">\n" +
    "						<label>Name</label>\n" +
    "						<input ng-model=\"name\">\n" +
    "					</md-input-container>\n" +
    "				</form>\n" +
    "			</md-content>\n" +
    "		</md-dialog-content>\n" +
    "		<md-dialog-actions layout=\"row\">\n" +
    "			<span flex></span>\n" +
    "			<md-button ng-click=\"cancel()\">\n" +
    "				Cancel\n" +
    "			</md-button>\n" +
    "			<md-button ng-click=\"add()\" >\n" +
    "				{{buttonLabel}}\n" +
    "			</md-button>\n" +
    "		</md-dialog-actions>\n" +
    "	</form>\n" +
    "</md-dialog>\n"
  );


  $templateCache.put('app/modules/layouts/main-page/import.html',
    "<md-dialog\n" +
    "	aria-label=\"Profile\">\n" +
    "	<form>\n" +
    "		<md-toolbar>\n" +
    "			<div class=\"md-toolbar-tools\">\n" +
    "				<h2>{{title}}</h2>\n" +
    "				<span flex></span>\n" +
    "				<md-button class=\"md-icon-button\" ng-click=\"cancel()\">\n" +
    "					<md-icon aria-label=\"Close dialog\">close</md-icon>\n" +
    "				</md-button>\n" +
    "			</div>\n" +
    "		</md-toolbar>\n" +
    "		<md-dialog-content style=\"width:600px;max-width:100%; max-height:810px; \">\n" +
    "			<md-content class=\"md-padding\">\n" +
    "				<md-input-container class=\"md-block\">\n" +
    "					<label>{{message}}</label>\n" +
    "					<textarea ng-model=\"data\" rows=\"5\" md-select-on-focus></textarea>\n" +
    "				</md-input-container>\n" +
    "			</md-content>\n" +
    "		</md-dialog-content>\n" +
    "		<md-dialog-actions layout=\"row\">\n" +
    "			<span flex></span>\n" +
    "			<md-button ng-if=\"type=='import'\" ng-click=\"cancel()\">\n" +
    "				Cancel\n" +
    "			</md-button>\n" +
    "			<md-button ng-if=\"type=='import'\" ng-click=\"import()\" >\n" +
    "				Import\n" +
    "			</md-button>\n" +
    "			<md-button ng-if=\"type=='export'\" ng-click=\"cancel()\">\n" +
    "				Ok\n" +
    "			</md-button>\n" +
    "		</md-dialog-actions>\n" +
    "	</form>\n" +
    "</md-dialog>\n"
  );

}]);
