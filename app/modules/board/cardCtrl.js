(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:cardCtrl
	 * @description
	 * # cardCtrl
	 * Controller of the card
	 */

	angular
		.module('kanbangular')
		.controller('CardCtrl', Card);

	Card.$inject = ['$scope', '$mdDialog', 'labelService', 'list', 'lists','item'];

	function Card($scope, $mdDialog, labelService,list, lists, item) {
		$scope.dialogtitle = "Add Card";
		$scope.buttonLabel = "Add";
		$scope.lists = lists;
		$scope.labels = labelService.labels;

		if (list){
			$scope.currentList = list
		}
		if (item){
			$scope.dialogtitle = "Edit Card";
			$scope.buttonLabel = "Save";
			$scope.title = item.title;
			$scope.description = item.description;
			$scope.currentLabels = item.labels;
		}
		$scope.cancel = function() {
			$mdDialog.cancel();
		};
		$scope.add = function() {
			$mdDialog.hide($scope);
		};
	}
})();
