(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.module:boardModule
	* @description
	* # boardModule
	* Module of the app
	*/

	angular.module('board', []);
})();
