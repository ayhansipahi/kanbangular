'use strict';

	/**
	* @ngdoc function
	* @name app.route:boardRoute
	* @description
	* # boardRoute
	* Route of the app
	*/

angular.module('kanbangular')
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider

			.state('board', {
				url: '',
				abstract: true,
				templateUrl: 'app/modules/board/board.html',
				controller: 'BoardCtrl',
				controllerAs: 'vm'
			})
			.state('board.dashboard', {
				url:'/dashboard',
				templateUrl: 'app/modules/board/dashboard.html'
			});

	}]);
