(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:boardService
	* @description
	* # boardService
	* Service of the app
	*/

	angular.module('kanbangular')
		.factory('boardService', boardService);

	boardService.$inject = ['lsService'];

	function boardService(lsService) {

		return {
			set: set,
			init: init
		};

		function set(data) {
			return lsService.set(data);
		}

		function init() {
			return lsService.get();
		}
	}

})();
