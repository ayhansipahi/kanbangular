(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:columnCtrl
	 * @description
	 * # columnCtrl
	 * Controller of the column
	 */

	angular
		.module('kanbangular')
		.controller('ColumnCtrl', Column);

	Column.$inject = ['$scope', '$mdDialog', 'name'];

	function Column($scope, $mdDialog, name) {
		if (name && name.length > 0){
			$scope.name = name;
			$scope.buttonLabel = "Save";
			$scope.dialogtitle = "Edit Column";
		} else {
			$scope.buttonLabel = "Add";
			$scope.dialogtitle = "Add Column";
		}
		$scope.cancel = function() {
			$mdDialog.cancel();
		};
		$scope.add = function(answer) {
			$mdDialog.hide($scope.name);
		};
	}
})();
