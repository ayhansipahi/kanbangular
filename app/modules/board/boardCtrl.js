(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:BoardCtrl
	* @description
	* # BoardCtrl
	* Controller of the app
	*/

	angular
		.module('kanbangular')
		.controller('BoardCtrl', Board);

	Board.$inject = ['$rootScope', '$scope', '$window','$mdDialog', '$mdToast', 'boardService'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Board($rootScope, $scope, $window, $mdDialog, $mdToast, boardService) {
		/*jshint validthis: true */

		var originatorEv;
		$scope.openMenu = function($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		$scope.listStyle = {
			height: ($window.innerHeight - 64) + 'px'
		};
		$window.addEventListener('resize', onResize);
		function onResize() {
			$scope.listStyle.height = ($window.innerHeight - 64) + 'px';
			if (!$scope.$root.$$phase) $scope.$digest();
		}

		$scope.models = boardService.init();


		$scope.data = JSON.stringify($scope.models);

		$scope.addData = function() {
			boardService.set(JSON.parse($scope.data));
			$scope.model = boardService.init();
		};

		/** List functions **/
		$scope.addList = function (ev) {
			$mdDialog.show({
				controller: 'ColumnCtrl',
				templateUrl: 'app/modules/board/dialogs/column.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals:{
					name: null
				}
			})
			.then(function(name) {
				var list = {
					id: (new Date().getTime()).toString(36),
					name: name,
					items: []
				};
				$scope.models.push(list);
			});
		};
		$scope.editList = function(ev, id){
			var list = $scope.models.filter(function(el,i) {
				return el.id == id;
			});
			$mdDialog.show({
				controller: 'ColumnCtrl',
				templateUrl: 'app/modules/board/dialogs/column.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals:{
					name: list[0].name
				}
			})
			.then(function(name) {
				list[0].name = name;
			});
		};

		$scope.deleteList = function(ev, data) {
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
				.title('Would you like to delete column '+data.name+'?')
				.textContent('All ('+ data.items.length +') cards in this column will be deleted.')
				.ariaLabel('Delete Column')
				.targetEvent(ev)
				.ok('Ok')
				.cancel('Cancel');
			$mdDialog.show(confirm).then(function() {
				$scope.models = $scope.models.filter(function(el, i) {
					return el.id !== data.id;
				});
			});
		};

		/** drag and drop functions **/
		$scope.getSelectedItemsIncluding = function(list, item) {
			item.selected = true;
			return list.items.filter(function(item) { return item.selected; });
		};

		$scope.onDragstart = function(list, event) {
			list.dragging = true;
			if (event.dataTransfer.setDragImage) {
				var img = new Image();
				img.src = 'app/assets/images/copy.png';
				event.dataTransfer.setDragImage(img, 0, 0);
			}
		};

		$scope.onDrop = function(list, items, index) {
			angular.forEach(items, function(item) { item.selected = false; });
			list.items = list.items.slice(0, index)
				.concat(items)
				.concat(list.items.slice(index));
			return true;
		};

		$scope.onMoved = function(list) {
			list.items = list.items.filter(function(item) { return !item.selected; });
		};

		/** Card Functions **/
		$scope.addItem = function(ev, list){
			$mdDialog.show({
				controller: 'CardCtrl',
				templateUrl: 'app/modules/board/dialogs/card.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals:{
					list: list,
					lists: $scope.models,
					item: false
				}
			})
			.then(function(scope) {
				var item = {
					id: (new Date().getTime()).toString(36),
					title: scope.title,
					description: scope.description,
					labels: scope.currentLabels,
					commends: []
				};
				$scope.models.filter(function(el,i) {
					return el.id == scope.currentList;
				})[0].items.push(item);
			});
		};
		$scope.editItem = function(ev, list, item){
			var listId = list;
			$mdDialog.show({
				controller: 'CardCtrl',
				templateUrl: 'app/modules/board/dialogs/card.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals:{
					list: list,
					lists: $scope.models,
					item: item
				}
			})
			.then(function(scope) {
				if (scope.currentList == listId){
					var orgItem = $scope.models.filter(function(el,i) {
						return el.id == listId;
					})[0].items.filter(function(el,i){
						return el.id == item.id;
					})[0];

					orgItem.title =  scope.title;
					orgItem.description= scope.description;
					orgItem.labels= scope.currentLabels;
				} else {
					angular.forEach($scope.models, function(el,i){
						el.items = el.items.filter(function(el, i) {
							return el.id !== item.id
						});
					});

					item.title =  scope.title;
					item.description= scope.description;
					item.labels= scope.currentLabels;

					$scope.models.filter(function(el,i) {
						return el.id == scope.currentList;
					})[0].items.push(item);
				}
			});
		};
		$scope.deleteItem = function(ev, item){
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
				.title('Would you like to delete card?')
				.textContent('All card in this column will be deleted.')
				.ariaLabel('Delete Column')
				.targetEvent(ev)
				.ok('Ok')
				.cancel('Cancel');
			$mdDialog.show(confirm).then(function() {
				angular.forEach($scope.models, function(el,i){
					el.items = el.items.filter(function(el, i) {
						return el.id !== item
					});
				});
			});
		}

		/** save on any change **/
		$scope.$watch('models', function(model) {
			boardService.set(model);
		}, true);

		$rootScope.$watch('models', function(val){
			$scope.models = boardService.init();
		});
	}
})();
