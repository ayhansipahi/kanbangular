(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:localstorageService
	 * @description
	 * # localstorageService
	 * Service of the app
	 */

	angular.module('kanbangular')
		.factory('lsService', lsService);

	lsService.$inject = ['localStorageService'];

	function lsService(localStorageService) {
		const key = "data";
		return {
			set: set,
			get: get
		};

		function set(data) {
			return localStorageService.set(key, JSON.stringify(data));
		}

		function get() {
			return  JSON.parse(localStorageService.get(key)) || [];
		}
	}

})();
