(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:labelService
	 * @description
	 * # labelService
	 * Service of the app
	 */

	angular.module('kanbangular')
		.factory('labelService', lsService);


	function lsService() {
		const labels = [
			{
				"name": "User Story",
				"color": "#01cc00"
			},
			{
				"name": "Defect",
				"color": "#fe0000"
			},
			{
				"name": "Task",
				"color": "#0065b3"
			},
			{
				"name": "Feature",
				"color": "#ff7f00"
			}
		];

		return {
			labels: labels
		};

	}

})();
