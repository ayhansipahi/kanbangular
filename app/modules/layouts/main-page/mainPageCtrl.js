(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:LayoutCtrl
	* @description
	* # LayoutCtrl
	* Controller of the app
	*/

	angular
		.module('kanbangular')
		.controller('LayoutCtrl', Layout);

	Layout.$inject = ['$rootScope', '$mdToast', '$mdDialog','lsService'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Layout($rootScope, $mdToast, $mdDialog, lsService) {
		/*jshint validthis: true */
		var vm = this;

		var originatorEv;
		vm.openMenu = function ($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		vm.export = function(ev){
			$mdDialog.show({
				controller: fileImport,
				templateUrl: 'app/modules/layouts/main-page/import.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals: {type:"export"}
			})
		};
		vm.import= function(ev){
			$mdDialog.show({
				controller: fileImport,
				templateUrl: 'app/modules/layouts/main-page/import.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals: {type:"import"}
			})
				.then(function(json) {
					try{
						var j = JSON.parse(json);
						lsService.set(j);
						$mdToast.showSimple("Data import successful.");
						$rootScope.models = j;
					} catch(err){
						$mdToast.showSimple(err.message);
					}
				});
		};

		fileImport.$inject = ['$scope', '$mdDialog','type'];

		function fileImport($scope, $mdDialog, type) {
			if(type == 'import'){
				$scope.title = "Import Data"
				$scope.message = "Paste your data here"
			} else {
				$scope.title = "Export Data"
				$scope.message = "Get your data here";
				$scope.data = angular.toJson(lsService.get());
			}
			$scope.type = type;
			$scope.cancel = function() {
				$mdDialog.cancel();
			};
			$scope.import = function() {
				$mdDialog.hide($scope.data);
			};
		}

	}

})();
