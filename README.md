#Kanbangular

Kanban Board with AngularJs and Material Design

## Getting Started

After clone the repo, 
Open your Terminal/Shell and type:

```bash
npm install
bower install
```

#### Installing bower-installer

```bash
npm install -g bower-installer
```
> The building process will use bower-installer plugin.

## Running project on development
Open your Terminal/Shell and type:

```bash
grunt dev
```

After the command your application should start right in your default browser at `localhost:4000`.

## Running project on production
Open your Terminal/Shell and type:

```bash
grunt build
```

The `Gruntfile.js` already have some tasks like: Concat, Uglify, Injector and template cache.

> NOTE: The command will concat and minify all (JS) application files and the HTML templates will be mixed in on file called `templates.js`, all this files will be injected on **index.html**.


### Notes

* Board data stored in the `localStorage`.
